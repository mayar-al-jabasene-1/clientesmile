import React from "react";
import "./Actions.scss";
import VisibilityIcon from "@mui/icons-material/Visibility";
import EditIcon from "@mui/icons-material/Edit";
import ModalMulti from "../ModalMulti/ModalMulti";
import ModalDelete from "../ModalDelete/ModalDelete";
import { useNavigate } from "react-router-dom";

function Actions({ filter, params }) {
  let navigate = useNavigate();
  let handlepath = (path) => {
    navigate(path);
  };
  return (
    <div className="action-icon">
      {filter === "users" ? (
        <div className="list-icons">
          {/*view */}
          {/* <div className="icon-view">
            <VisibilityIcon />
          </div> */}
          {/*edit */}
          <div className="icon-edit">
            <ModalMulti params={params} filter="users" type="edit" />
          </div>
          {/*delte */}
          <div className="icon-delete">
            <ModalDelete filter="users" params={params} />
          </div>
        </div>
      ) : (
        ""
      )}
    </div>
  );
}

export default Actions;
