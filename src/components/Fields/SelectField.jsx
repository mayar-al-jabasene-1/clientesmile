import * as React from "react";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormHelperText from "@mui/material/FormHelperText";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";

export default function SelectField({ values, title, getdata }) {
  const [val, setVal] = React.useState("");

  const handleChange = (event) => {
    setVal(event.target.value);
    getdata(event);
  };

  return (
    <div>
      <FormControl sx={{ m: 1, minWidth: 120 }}>
        <InputLabel id="demo-simple-select-helper-label">{title}</InputLabel>
        <Select
          labelId="demo-simple-select-helper-label"
          id="demo-simple-select-helper"
          value={val}
          label={title}
          onChange={handleChange}
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          {values &&
            values.map((e) => {
              return (
                <MenuItem value={e.val}>{e.name ? e.name : e.title}</MenuItem>
              );
            })}
        </Select>
      </FormControl>
    </div>
  );
}
