import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import { useSelector } from "react-redux";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import DoNotDisturbAltIcon from "@mui/icons-material/DoNotDisturbAlt";
import {
  CircularProgress,
  DialogTitle,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  Switch,
  TextField,
  Typography,
} from "@mui/material";
import CircularIndeterminate from "../../../components/CircularIndeterminate/CircularIndeterminate";
import ErrorCompo from "../../../components/ErrorCompo/ErrorCompo";
import { DatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { notifyError } from "../../../Notification";
import { useDispatch } from "react-redux";
import {
  addPayment,
  fetchAppointmentById,
} from "../../../store/appointmentsSlice";

function AddPayment({ type, onClose, data }) {
  const [t, i18n] = useTranslation();
  const ErrorFetchPayment = false;
  const loadingPayment = false;
  let { loadingBTN } = useSelector((state) => state.persistData.appointments);

  let [inp, setInp] = useState({
    active: 1,
    status: "PENDDING",
    appointment_detail_id: data,
    cuurency_id: "1",
  });

  console.log("inpinpinp", inp);

  const handleChangeDate = (e, name) => {
    setInp((prev) => {
      return {
        ...prev,
        selected_datetime: e.format("YYYY-MM-DD"),
      };
    });
  };

  let getValue = (e) => {
    setInp((prev) => {
      return { ...prev, [e.target.name]: e.target.value };
    });
  };

  let dispatch = useDispatch();
  const onsubmitfn = (e) => {
    e.preventDefault();
    if (type === "edit") {
      // Update appointment
    } else {
      dispatch(addPayment({ data: inp, lang: i18n.language })).then(
        (response) => {
          if (response.payload.success) {
            dispatch(fetchAppointmentById({ id: data }));
            onClose();
          } else {
            notifyError("Failed to add Payment. Please try again.");
          }
        }
      );
    }
  };

  return (
    <>
      <DialogTitle>
        <Typography
          variant="h6"
          gutterBottom
          style={{ display: "flex", justifyContent: "space-between" }}
        >
          {type === "edit"
            ? t("Edit Payment")
            : type === "view"
            ? t("View Payment")
            : t("Add Payment")}
        </Typography>
      </DialogTitle>
      <>
        {ErrorFetchPayment ? (
          <ErrorCompo />
        ) : loadingPayment ? (
          <div className="loading">
            <CircularIndeterminate />
          </div>
        ) : (
          <form class="row" onSubmit={(e) => onsubmitfn(e)}>
            <div className="col-lg-12">
              <div className="form-group mb-4 mt-4">
                <TextField
                  id={"text"}
                  type={"number"}
                  label={t("Total")}
                  name={"total"}
                  required
                  onChange={(e) => getValue(e)}
                  variant="outlined"
                  style={{ width: "100%" }}
                />
              </div>
            </div>
            <div className="col-lg-12">
              <LocalizationProvider dateAdapter={AdapterDayjs}>
                <InputLabel id="Date">{t("Date")}:</InputLabel>
                <DatePicker
                  onChange={(newValue) => {
                    handleChangeDate(newValue, "selected_datetime");
                  }}
                  defaultValue={null}
                />
              </LocalizationProvider>
            </div>

            {/* <div className="col-lg-12">
              <div className="form-group mb-4 mt-4">
                <TextField
                  id={"text"}
                  type={"text"}
                  label={t("Cost")}
                  name={"cost"}
                  //   required
                  onChange={(e) => getValue(e)}
                  variant="outlined"
                  style={{ width: "100%" }}
                />
              </div>
            </div> */}

            <div className="col-lg-12">
              <div className="form-group mb-4 mt-4">
                <FormControl fullWidth required>
                  <InputLabel id="status">{t("Payment method")}</InputLabel>
                  <Select
                    labelId="payment method"
                    name="payment_method"
                    id="payment_method"
                    required
                    label="Payment method"
                    value={inp.payment_method}
                    onChange={(e) => getValue(e)}
                  >
                    <MenuItem value={"CASH"}>CASH</MenuItem>
                    <MenuItem value={"CARD"}>CARD</MenuItem>
                    <MenuItem value={"TRANSFER"}>TRANSFER</MenuItem>
                  </Select>
                </FormControl>
              </div>
            </div>
            {/* <div className="col-lg-12">
              <div className="form-group mb-4 mt-4">
                <FormControl fullWidth required>
                  <InputLabel id="status">{t("Status")}</InputLabel>
                  <Select
                    labelId="Status"
                    name="status"
                    id="status"
                    required
                    label="status"
                    value={inp.status}
                    onChange={(e) => getValue(e)}
                  >
                    <MenuItem value={"PENDDING"}>PENDING</MenuItem>
                    <MenuItem value={"DONE"}>DONE</MenuItem>
                    <MenuItem value={"CANCELED"}>CANCELED</MenuItem>
                  </Select>
                </FormControl>
              </div>
            </div> */}

            <div className="btn-lest mt-3">
              {loadingBTN ? (
                <button type="text" disabled className="btn btn-primary-rgba ">
                  <CheckCircleIcon color="#fff" /> {t("Loading")}...
                </button>
              ) : (
                <button type="submit" className="btn btn-primary-rgba">
                  <CheckCircleIcon color="#fff" />
                  {type === "edit" ? t("Edit") : t("Create")}
                </button>
              )}

              <span onClick={onClose} className="btn btn-danger-rgba">
                <DoNotDisturbAltIcon color="#fff" /> {t("Cancel")}
              </span>
            </div>
          </form>
        )}
      </>
    </>
  );
}

export default AddPayment;
