// src/slices/doctorsSlice.js
import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import { BASE_URL } from "../apiConfig";
import { notifyError, notifysuccess } from "../Notification";

const API_URL = "/cors/doctors";

// Async Thunks for doctor operations with headers and ThunkAPI for token and notifications
export const fetchDoctors = createAsyncThunk(
  "doctors/fetchDoctors",
  async (arg, ThunkAPI) => {
    const { rejectWithValue, getState } = ThunkAPI;
    const token = getState().persistData.auth.data.token;
    try {
      const response = await axios.get(`${BASE_URL}${API_URL}`, {
        headers: {
          Authorization: `Bearer ${token}`,
          lang: arg.lang,
        },
      });
      console.log("response.data.result", response.data.result);
      return response.data.result.doctors.data;
    } catch (error) {
      return rejectWithValue(
        error.response ? error.response.data : error.message
      );
    }
  }
);

export const fetchDoctorById = createAsyncThunk(
  "doctors/fetchDoctorById",
  async (arg, ThunkAPI) => {
    const { rejectWithValue, getState } = ThunkAPI;
    const token = getState().persistData.auth.data.token;
    try {
      const response = await axios.get(`${BASE_URL}${API_URL}/${arg.id}`, {
        headers: {
          Authorization: `Bearer ${token}`,
          lang: arg.lang,
        },
      });
      console.log("response.datat", response.data);
      return response.data.result.item;
    } catch (error) {
      return rejectWithValue(
        error.response ? error.response.data : error.message
      );
    }
  }
);

export const createDoctor = createAsyncThunk(
  "doctors/createDoctor",
  async (arg, ThunkAPI) => {
    const { rejectWithValue, getState } = ThunkAPI;
    const token = getState().persistData.auth.data.token;
    try {
      const response = await axios.post(`${BASE_URL}${API_URL}`, arg.data, {
        headers: {
          "Content-Type": "multipart/form-data",
          Authorization: `Bearer ${token}`,
          lang: arg.lang,
        },
      });
      // notifysuccess(response.data.message);
      return response.data;
    } catch (error) {
      notifyError(error.response ? error.response.data.message : error.message);
      return rejectWithValue(
        error.response ? error.response.data : error.message
      );
    }
  }
);

export const updateDoctor = createAsyncThunk(
  "doctors/updateDoctor",
  async (arg, ThunkAPI) => {
    const { rejectWithValue, getState } = ThunkAPI;
    const token = getState().persistData.auth.data.token;
    try {
      const response = await axios.post(
        `${BASE_URL}${API_URL}/${arg.id}`,
        arg.data,
        {
          headers: {
            "Content-Type": "multipart/form-data",
            // "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
            lang: arg.lang,
          },
        }
      );
      // notifysuccess(response.data.message);
      return response.data;
    } catch (error) {
      notifyError(error.response ? error.response.data.message : error.message);
      return rejectWithValue(
        error.response ? error.response.data : error.message
      );
    }
  }
);

export const deleteDoctor = createAsyncThunk(
  "doctors/deleteDoctor",
  async (arg, ThunkAPI) => {
    const { rejectWithValue, getState } = ThunkAPI;
    const token = getState().persistData.auth.data.token;
    try {
      const idList = Array.isArray(arg.id) ? arg.id : [arg.id]; // Ensure id is an array
      const promises = idList.map((id) =>
        axios.post(`${BASE_URL}/cors/doctors/${id}`, arg.data, {
          headers: {
            "Content-Type": "multipart/form-data",
            Authorization: `Bearer ${token}`,
            lang: arg.lang,
          },
        })
      );

      // Wait for all deletion requests to complete
      const responses = await Promise.all(promises); // Wait for all deletion requests to complete

      return { idList: idList, responses: responses }; // Return the list of deleted IDs
    } catch (error) {
      notifyError(error.response ? error.response.data.message : error.message);
      return rejectWithValue(
        error.response ? error.response.data : error.message
      );
    }
  }
);

const doctorsSlice = createSlice({
  name: "doctors",
  initialState: {
    doctors: [],
    doctor: null,
    dataCreate: false,
    loading: false,
    error: null,
    dataUpdate: false,
    roles: false,
    loadingSingle: false,
    singleData: false,
    loadingBTN: false,
    errorAll: false,
    errorSingle: false,
  },
  reducers: {},
  extraReducers: (builder) => {
    builder
      // Fetch all doctors
      .addCase(fetchDoctors.pending, (state) => {
        state.loading = true;
        state.error = null;
      })
      .addCase(fetchDoctors.fulfilled, (state, action) => {
        state.loading = false;
        console.log("plqpwlpqwldpqw", action.payload);
        state.doctors = action.payload;
      })
      .addCase(fetchDoctors.rejected, (state, action) => {
        state.loading = false;
        state.error = action.payload;
      })
      // Fetch doctor by ID
      .addCase(fetchDoctorById.pending, (state) => {
        state.loadingSingle = true;
        state.error = null;
      })
      .addCase(fetchDoctorById.fulfilled, (state, action) => {
        state.loadingSingle = false;
        state.singleData = action.payload;
      })
      .addCase(fetchDoctorById.rejected, (state, action) => {
        state.loadingSingle = false;
        state.error = action.payload;
      })
      // Create a new doctor
      .addCase(createDoctor.pending, (state) => {
        state.loadingBTN = true;
        state.error = null;
      })
      .addCase(createDoctor.fulfilled, (state, action) => {
        state.loadingBTN = false;
        state.dataCreate = action.payload;
        notifysuccess(action.payload.message);
      })
      .addCase(createDoctor.rejected, (state, action) => {
        state.loadingBTN = false;
        state.error = action.payload;
        notifyError(
          action.payload?.message && action.payload.message
            ? action.payload.message
            : action.payload
        );
      })
      // Update a doctor
      .addCase(updateDoctor.pending, (state) => {
        state.loadingBTN = true;
        state.error = null;
      })
      .addCase(updateDoctor.fulfilled, (state, action) => {
        state.loadingBTN = false;
        state.dataUpdate = action.payload;
        state.error = false;
        notifysuccess(action.payload.message);
      })
      .addCase(updateDoctor.rejected, (state, action) => {
        state.loadingBTN = false;
        state.error = action.payload;
        notifyError(
          action.payload?.message && action.payload.message
            ? action.payload.message
            : action.payload
        );
      })
      // Delete a doctor
      .addCase(deleteDoctor.pending, (state) => {
        state.loadingBTN = true;
        state.error = false;
      })
      .addCase(deleteDoctor.fulfilled, (state, action) => {
        state.doctors = state.doctors.filter(
          (e) => !action.payload.idList.includes(e.id)
        );
        state.loadingBTN = false;
        state.error = false;
        notifysuccess(action.payload.responses[0].data.message);
      })
      .addCase(deleteDoctor.rejected, (state, action) => {
        state.loadingBTN = false;
        state.errorDelete = action.payload;
        notifyError(
          action.payload?.message && action.payload.message
            ? action.payload.message
            : action.payload
        );
      });
  },
});

export default doctorsSlice.reducer;
